
import React, { Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Keyboard,
    ImageBackground,
    Dimensions,
    TouchableOpacity,
    Alert
} from 'react-native';
import font from "./../../Utils/fontSize"
import {
    Header,
    LearnMoreLinks,
    Colors,
    DebugInstructions,
    ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import TextFeild from "./../../Component/TextField"
import RippleButton from "./../../Component/RippleButton"
import Fontisto from 'react-native-vector-icons/Fontisto';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import { postAipCall } from "./../../Network/ApiRequest"
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
const screenHeight = Dimensions.get('window').height;
import { API_URL } from "./../../Network/Url"
import QRCodeScanner from 'react-native-qrcode-scanner';
import { RNCamera } from 'react-native-camera';
import HeaderScreens from "./../../Component/Header";
import NetInfo from "@react-native-community/netinfo";
import LoadingIndicator from "./../../Component/Loader"
import Toast from 'react-native-simple-toast';


class HomeComponents extends Component {

     componentDidMount =()=>{
        this.lisener()
     }

     lisener = async () => {
        this.willFocusListener = this.props.navigation.addListener('focus', () => {
            this.scanner.reactivate();
        })
      }
    

  

    onSuccess = e => {
        const uservalue = JSON.parse(e.data)
        setTimeout(() => {
            this.getApiDetails(uservalue) 
        }, 500);
        
        // Linking.openURL(e.data).catch(err =>
        //     console.error('An error occured', err)
        // );
    };

    getApiDetails = async (value) => {
        console.log("value323525554354",value.userId)
        const user = await AsyncStorage.getItem('userDetails')
        const userDetail = JSON.parse(user);
        console.log("userDetail===>", userDetail.response_data.authtoken);
        const cost_user_id = value.userId
        let object = { "user_id": cost_user_id }
        NetInfo.fetch().then(isConnected => {
            if (isConnected) {
                fetch(`${API_URL}` + "store/get-user", {
                    method: 'POST', // or 'PUT'
                    headers: {
                        Accept: "application/json",
                         "Content-Type": "application/json",
                        "x-access-token": userDetail.response_data.authtoken
                    },
                    body: JSON.stringify(object),
                })
                    .then(response => response.json())
                    .then(data => {
                        console.log('Success:', data);
                        if (data.response_code == 2000) {
                            this.setState({
                                isLoading: false
                            })
                              this.props.navigation.navigate("customerScreens",{navUserDetails:data.response_data})
                        } else {
                            this.setState({
                                isLoading: false
                            })
                            Toast.show("" + data.response_message)
                        }
                    })
                    .catch((error) => {
                        console.error('Error:', error);
                    });
            }

        })
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <HeaderScreens
                    headerTitle={"Scanner"}
                    iconName={"bars"}
                    backPrees={() => this.props.navigation.openDrawer()}
                />
                <ImageBackground
                    source={require("./../../Images/backGround.png")}
                    style={{ flex: 1 }}
                    resizeMode={"cover"}
                >

                    <ScrollView showsVerticalScrollIndicator={false}>
                        <View style={{ margin: 25 }}>
                            {/* <View style={{ height: screenHeight / 8, justifyContent: "center", alignItems: "center" }}>
                                <Text style={{ color: Colors.white, fontSize: font.large_size }}>Vivek kumar</Text>
                                <Text style={{ color: Colors.white, fontSize: font.medium_size, paddingTop: 10 }}>vivek.brainium@gmail.com</Text>
                            </View> */}
                            <View style={{ justifyContent: "center", alignItems: "center" }}>
                                <QRCodeScanner
                                    onRead={this.onSuccess}
                                    ref={(node) => { this.scanner = node }}
                                    containerStyle={{ height: screenHeight / 1.2 }}
                                    // flashMode={RNCamera.Constants.FlashMode.torch}
                                    bottomContent={
                                        <TouchableOpacity style={styles.buttonTouchable}>
                                            <Text style={styles.buttonText}>OK. Got it!</Text>
                                        </TouchableOpacity>
                                    }
                                />
                            </View>
                        </View>



                    </ScrollView>
                </ImageBackground>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    centerText: {
        flex: 1,
        fontSize: 18,
        padding: 32,
        color: '#777'
    },
    textBold: {
        fontWeight: '500',
        color: '#000'
    },
    buttonText: {
        fontSize: 21,
        color: 'rgb(0,122,255)'
    },
    buttonTouchable: {
        padding: 16
    }
});

export default HomeComponents;