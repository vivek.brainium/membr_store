
import React, { Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Keyboard,
    ImageBackground,
    Dimensions,
    TouchableOpacity,
    Alert
} from 'react-native';
import font from "./../../Utils/fontSize"
import {
    Header,
    LearnMoreLinks,
    Colors,
    DebugInstructions,
    ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import TextFeild from "./../../Component/TextField"
import RippleButton from "./../../Component/RippleButton"
import Fontisto from 'react-native-vector-icons/Fontisto';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import { postAipCall } from "./../../Network/ApiRequest"
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
const screenHeight = Dimensions.get('window').height;
import Apis from "../../Network/ApiCall";
import NetInfo from "@react-native-community/netinfo";
import { API_URL } from "./../../Network/Url"
import LoadingIndicator from "./../../Component/Loader"
import Toast from 'react-native-simple-toast';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            isLoading: false
        };
    }

    handleforgotPassword() {
        this.props.navigation.navigate("forgotPassword")
    }

    hanhleSubmit = () => {

        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (this.state.username.length == 0) {
            Toast.show("Email Address is require")
        } else if (this.state.password.length == 0) {
            Toast.show("Password is require")
        } else if (reg.test(this.state.username) == false) {
            Toast.show('Invalid Email Address');
            return false;
        } else {
            this.setState({ isLoading: true })
            var object = {
                "email": this.state.username,
                "password": this.state.password
            }
            // console.log("test==>",API_URL);

            NetInfo.fetch().then(isConnected => {
                if (isConnected) {
                    fetch(`${API_URL}` + "store/login", {
                        method: 'POST', // or 'PUT'
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json"
                        },
                        body: JSON.stringify(object),
                    })
                        .then(response => response.json())
                        .then(data => {
                            console.log('Success:', data);
                            if (data.response_code == 2000) {
                                this.setState({
                                    isLoading: false
                                })
                                Toast.show("" + data.response_message)
                                this.handleUserDetails(data)
                            } else {
                                this.setState({
                                    isLoading: false
                                })
                                Toast.show("" + data.response_message)
                            }
                        })
                        .catch((error) => {
                            console.error('Error:', error);
                        });
                }

            })
        }
    }

    handleUserDetails = async (value) => {
        console.log("value user login ----->", value);
        try {
            await AsyncStorage.setItem('userDetails', JSON.stringify(value))
            // await AsyncStorage.setItem('token', value.response_data.authtoken)
            // if (value.response_code == 2000) {
                this.props.navigation.replace("drawerComponents")
            // }
        } catch (e) {
            // saving error
        }
    }

    render() {

        return (
            <View style={{ flex: 1 }}>
                <ImageBackground
                    source={require("./../../Images/backGround.png")}
                    style={{ flex: 1 }}
                    resizeMode={"cover"}
                >
                    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                        <ScrollView>
                            <View style={{ height: screenHeight, paddingHorizontal: 25 }}>
                                <View style={{ height: screenHeight / 5, justifyContent: "center", }}>
                                    <Text style={{ fontSize: font.extra_large, color: Colors.white, fontWeight: "700" }}>Sign in</Text>
                                </View>

                                <View style={{ height: screenHeight / 2.5, justifyContent: "center" }}>
                                    <View style={{ backgroundColor: "#003a2e", paddingLeft: 10, height: screenHeight / 10, justifyContent: "center", borderRadius: 5 }}>
                                        <TextFeild
                                            placeholder={"Enter your email"}
                                            placeholderTextColor={"#999999"}
                                            onChangeText={(val) => this.setState({ username: val })}
                                            secureTextEntry={false}
                                            icon={true}
                                            iconName={Fontisto}
                                            iconType={"email"}
                                            size={20}
                                        />
                                    </View>

                                    <View style={{ height: 0.3, backgroundColor: Colors.white }} />

                                    <View style={{ backgroundColor: "#003a2e", height: screenHeight / 10, justifyContent: "center", borderRadius: 5 }}>
                                        <TextFeild
                                            placeholder={"Enter your password"}
                                            placeholderTextColor={"#999999"}
                                            onChangeText={(val) => this.setState({ password: val })}
                                            secureTextEntry={true}
                                            icon={true}
                                            iconName={EvilIcons}
                                            iconType={"unlock"}
                                            size={35}
                                        />
                                    </View>

                                    <TouchableOpacity
                                        style={{ paddingVertical: 20, alignItems: "flex-end" }}
                                        onPress={() => this.handleforgotPassword()}
                                    >
                                        <Text style={{ color: Colors.white, fontSize: font.small_size }}>Forgot Password?</Text>
                                    </TouchableOpacity>


                                    <RippleButton pressInColor={"#008e6f"} style={{ height: screenHeight / 12, borderRadius: 5, alignItems: "center", justifyContent: "center", backgroundColor: "#008e6f" }}
                                        onPress={() => this.hanhleSubmit()}
                                    >
                                        <View
                                            style={[styles.containerPrimary, { backgroundColor: '#008e6f' }]}
                                        >
                                            <Text style={[styles.textPrimary, { color: Colors.white }]}>{"Login"}</Text>
                                        </View>
                                    </RippleButton>

                                </View>
                            </View>
                        </ScrollView>
                    </TouchableWithoutFeedback>
                    <LoadingIndicator loading={this.state.isLoading} />
                </ImageBackground>
            </View>

        );
    }
};

const styles = StyleSheet.create({
    scrollView: {
        backgroundColor: Colors.lighter,
    },
    engine: {
        position: 'absolute',
        right: 0,
    },
    body: {
        backgroundColor: Colors.white,
    },
    sectionContainer: {
        marginTop: 32,
        paddingHorizontal: 24,
    },
    sectionTitle: {
        fontSize: 24,
        fontWeight: '600',
        color: Colors.black,
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
        color: Colors.dark,
    },
    highlight: {
        fontWeight: '700',
    },
    footer: {
        color: Colors.dark,
        fontSize: 12,
        fontWeight: '600',
        padding: 4,
        paddingRight: 12,
        textAlign: 'right',
    },

    /////////////


    containerPrimary: {
        backgroundColor: '#fff',
        // paddingVertical: 12,
        paddingHorizontal: 10,
        justifyContent: 'center',
        alignItems: 'center',
        // borderRadius: 30,
    },
    textPrimary: {
        // color: '#00ADEF',
        fontSize: 18,
        // fontFamily: FontFamily.Poppins_Regular,
        // fontFamily: Fonts.fontPrimary,
        fontWeight: 'bold'
    },

});

export default Login;