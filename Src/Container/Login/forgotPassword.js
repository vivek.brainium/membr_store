
import React, { Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Keyboard,
    ImageBackground,
    Dimensions,
    TouchableOpacity,
    
} from 'react-native';
import font from "./../../Utils/fontSize"
import {
    Header,
    LearnMoreLinks,
    Colors,
    DebugInstructions,
    ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import TextFeild from "./../../Component/TextField"
import RippleButton from "./../../Component/RippleButton"
const screenHeight = Dimensions.get('window').height;
import Ionicons from 'react-native-vector-icons/Ionicons';
import Fontisto from 'react-native-vector-icons/Fontisto';
import NetInfo from "@react-native-community/netinfo";
import { API_URL } from "./../../Network/Url"
import LoadingIndicator from "./../../Component/Loader"
import Toast from 'react-native-simple-toast';
class ForgotPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            isLoading: false
        };
    }


    hanhleSubmit() {
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

        if (this.state.email.length == 0) {
            Toast.show("Email is required")
        } else if (reg.test(this.state.email) == false) {
            Toast.show("Email is required")
                ('Invalid Email Address');
            return false;
        } else {
            this.setState({ isLoading: true })
            const object = {
                "email": this.state.email
            }
            NetInfo.fetch().then(isConnected => {
                if (isConnected) {
                    fetch(`${API_URL}` + "store/forgot-password", {
                        method: 'POST', // or 'PUT'
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json"
                        },
                        body: JSON.stringify(object),
                    })
                        .then(response => response.json())
                        .then(data => {
                            console.log(' forgote success:', data);
                            if (data.response_code == 2000) {
                                this.setState({
                                    isLoading: false
                                })
                                Toast.show("" + data.response_message)
                                this.props.navigation.navigate("Login")
                            } else {
                                this.setState({
                                    isLoading: false
                                })
                                Toast.show("" + data.response_message)
                            }
                        })
                        .catch((error) => {
                            console.error('Error:', error);
                        });
                }

            })
        }
    }


    render() {
        return (
            <View style={{ flex: 1 }}>
                <ImageBackground
                    source={require("./../../Images/backGround.png")}
                    style={{ flex: 1 }}
                    resizeMode={"cover"}
                >
                    <Ionicons
                        onPress={() => this.props.navigation.goBack()}
                        style={{ padding: 20 }}
                        name="md-arrow-back"
                        color="#fff"
                        size={30}
                    />

                    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                        <ScrollView>
                            <View style={{ height: screenHeight, paddingHorizontal: 25 }}>

                                <View style={{ height: screenHeight / 7, justifyContent: "center", }}>
                                    <Text style={{ fontSize: font.extra_large, color: Colors.white, fontWeight: "700" }}>Forgot Password</Text>
                                </View>

                                <View style={{ backgroundColor: "#003a2e", height: screenHeight / 10, justifyContent: "center", borderRadius: 5, paddingLeft: 10 }}>


                                    <TextFeild
                                        placeholder={"Enter your email"}
                                        placeholderTextColor={"#999999"}
                                        onChangeText={(val) => this.setState({ email: val })}
                                        secureTextEntry={false}
                                        icon={true}
                                        iconName={Fontisto}
                                        iconType={"email"}
                                        size={20}
                                    />
                                </View>

                                {/* <View style={{ height: }}> */}
                                <RippleButton pressInColor={"#008e6f"} style={{ height: screenHeight / 12, borderRadius: 5, alignItems: "center", justifyContent: "center", backgroundColor: "#008e6f", marginTop: 30 }}
                                    onPress={() => this.hanhleSubmit()}
                                >
                                    <View
                                        style={[styles.containerPrimary, { backgroundColor: '#008e6f' }]}
                                    >
                                        <Text style={[styles.textPrimary, { color: Colors.white }]}>{"Send"}</Text>
                                    </View>
                                </RippleButton>
                            </View>
                        </ScrollView>
                    </TouchableWithoutFeedback>
                    <LoadingIndicator loading={this.state.isLoading} />
                </ImageBackground>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    scrollView: {
        backgroundColor: Colors.lighter,
    },
    engine: {
        position: 'absolute',
        right: 0,
    },
    body: {
        backgroundColor: Colors.white,
    },
    sectionContainer: {
        marginTop: 32,
        paddingHorizontal: 24,
    },
    sectionTitle: {
        fontSize: 24,
        fontWeight: '600',
        color: Colors.black,
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
        color: Colors.dark,
    },
    highlight: {
        fontWeight: '700',
    },
    footer: {
        color: Colors.dark,
        fontSize: 12,
        fontWeight: '600',
        padding: 4,
        paddingRight: 12,
        textAlign: 'right',
    },

    /////////////


    containerPrimary: {
        backgroundColor: '#fff',
        // paddingVertical: 12,
        paddingHorizontal: 10,
        justifyContent: 'center',
        alignItems: 'center',
        // borderRadius: 30,
    },
    textPrimary: {
        // color: '#00ADEF',
        fontSize: 18,
        // fontFamily: FontFamily.Poppins_Regular,
        // fontFamily: Fonts.fontPrimary,
        fontWeight: 'bold'
    },

});

export default ForgotPassword;