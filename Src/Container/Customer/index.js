
import React, { Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Keyboard,
    ImageBackground,
    Dimensions,
    TouchableOpacity,
    Alert
} from 'react-native';
import font from "./../../Utils/fontSize"
import {
    Header,
    LearnMoreLinks,
    Colors,
    DebugInstructions,
    ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import TextFeild from "./../../Component/TextField"
import RippleButton from "./../../Component/RippleButton"
import { postAipCall } from "./../../Network/ApiRequest"
import AsyncStorage from '@react-native-community/async-storage';
const screenHeight = Dimensions.get('window').height;
import HeaderScreens from "./../../Component/Header"
import NetInfo from "@react-native-community/netinfo";
import { API_URL } from "./../../Network/Url"
import LoadingIndicator from "./../../Component/Loader"
import Toast from 'react-native-simple-toast';
// import TextFeild from "./../../Component/TextField"


class CustomerComponents extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            isLoading: true,
            costumerData: [],
            user_id: "",
            totalAmount: "",
            token: "", paidAmount: ""
        };
    }

    componentDidMount() {
        console.log("hello-----dsldsfgsdfg", this.props.route.params.navUserDetails)
        this.getApiCalling()
    }

    getApiCalling = async () => {
        const user = await AsyncStorage.getItem('userDetails')
        let userDetail = JSON.parse(user);
        this.setState({
            user_id: userDetail.response_data._id,
            token: userDetail.response_data.authtoken
        })
        console.log("userDetail===>", userDetail);
        const cost_user_id = this.props.route.params.navUserDetails._id
        var MyDate = new Date();
        let months = MyDate.getMonth();
        NetInfo.fetch().then(isConnected => {


            if (isConnected) {

                fetch(`${API_URL}` + "user-discount?page=1&limit=10&user_id=" + cost_user_id + "&month=" + months, {
                    method: 'GET', // or 'PUT'
                    headers: {
                        Accept: "application/json",
                        "Content-Type": "application/json",
                        "x-access-token": userDetail.response_data.authtoken
                    },
                    // body: JSON.stringify(object),
                })
                    .then(response => response.json())
                    .then(data => {
                        console.log('Success:', data);
                        if (data.response_code == 2000) {
                            this.setState({
                                isLoading: false,
                                costumerData: (data.response_data.docs.lenght != 0) ? data.response_data.docs[0] : []
                            })
                            // Toast.show("" + data.response_message)
                        } else {
                            this.setState({
                                isLoading: false
                            })
                            Toast.show("" + data.response_message)
                        }
                    })
                    .catch((error) => {
                        console.error('Error:', error);
                    });
            }

        })
    }


    handleSubmit = async () => {
        // alert("testing", this.state.user_id)
        if (this.state.totalAmount.length == 0) {
            Toast.show("Amount is require")
        } else {

            this.setState({ isLoading: true })
            var MyDate = new Date();
            let months = MyDate.getMonth();
            const cost_user_id = this.props.route.params.navUserDetails._id
            let object = {
                "user_id": cost_user_id,
                "store_biller_id": this.state.user_id,
                "discount": (this.state.costumerData.length == 0) ? "no" : "yes",
                "discount_details": {
                    "month": months,
                    "discount_percent": this.state.costumerData.discount_percent
                },
                "total_amount": this.state.totalAmount
            }

            console.log("object===>", object);

            NetInfo.fetch().then(isConnected => {
                if (isConnected) {
                    fetch(`${API_URL}` + "store/order", {
                        method: 'POST', // or 'PUT'
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            "x-access-token": this.state.token
                        },
                        body: JSON.stringify(object),
                    })
                        .then(response => response.json())
                        .then(data => {
                            console.log('Success11111:', data);
                            if (data.response_code == 2000) {
                                let savingAmount = parseInt(this.state.totalAmount - this.per(this.state.costumerData.discount_percent, this.state.totalAmount))
                                alert(savingAmount)
                                this.setState({
                                    isLoading: false,
                                    paidAmount: savingAmount
                                    // costumerData: data.response_data.docs[0]
                                })
                                Toast.show("" + data.response_message)
                            } else {
                                this.setState({
                                    isLoading: false
                                })
                                Toast.show("" + data.response_message)
                            }
                        })
                        .catch((error) => {
                            console.error('Error:', error);
                        });
                }

            })
        }
    }

    per(num, amount) {
        return num * amount / 100;
    }




    render() {
        return (
            <View style={{ flex: 1 }}>
                <HeaderScreens
                    headerTitle={"Customer details"}
                    iconName={"arrowleft"}
                    backPrees={() => this.props.navigation.goBack()}
                />
                <ImageBackground
                    source={require("./../../Images/backGround.png")}
                    style={{ flex: 1 }}
                    resizeMode={"cover"}
                >

                    <ScrollView showsVerticalScrollIndicator={false}>
                        <View style={{ margin: 25, height: screenHeight }}>
                            <View style={{ height: screenHeight / 10, justifyContent: "center", alignItems: "center" }}>
                                <Text style={{ fontSize: font.large_size, color: Colors.white }}>{this.props.route.params.navUserDetails.name}</Text>
                                <Text style={{ fontSize: font.medium_size, color: Colors.white, paddingTop: 10 }}>{this.props.route.params.navUserDetails.email}</Text>
                            </View>
                            <View style={{ height: screenHeight / 10, justifyContent: "center", alignItems: "center", paddingTop: 20 }}>
                                <Text style={{ fontSize: font.small_size, color: Colors.white }}>Special discount offer<Text style={{ fontSize: font.extra_large, color: Colors.white }}> {this.state.costumerData.discount_percent + "%"} </Text> {"off"} </Text>
                            </View>
                            <View style={{ height: screenHeight / 5, justifyContent: "center" }}>
                                <View style={{ backgroundColor: "#003a2e", paddingLeft: 10, height: screenHeight / 10, justifyContent: "center", borderRadius: 5 }}>
                                    <TextFeild
                                        placeholder={"Enter customer amount"}
                                        placeholderTextColor={"#999999"}
                                        onChangeText={(val) => this.setState({ totalAmount: val })}
                                        secureTextEntry={false}
                                        icon={false}
                                    // iconName={Fontisto}
                                    // iconType={"email"}
                                    // size={20}
                                    />
                                </View>
                            </View>

                            <RippleButton pressInColor={"#008e6f"} style={{ height: screenHeight / 12, borderRadius: 5, alignItems: "center", justifyContent: "center", backgroundColor: "#008e6f" }}
                                onPress={() => this.handleSubmit()}
                            >
                                <View
                                    style={[styles.containerPrimary, { backgroundColor: '#008e6f' }]}
                                >
                                    <Text style={[styles.textPrimary, { color: Colors.white }]}>{"Save"}</Text>
                                </View>
                            </RippleButton>


                            <View style={{ paddingTop: 30, justifyContent: "center" }}>
                                <Text style={{ color: Colors.white, fontSize: font.large_size }}>Total amount :- {this.state.paidAmount}</Text>
                            </View>
                        </View>
                    </ScrollView>
                    <LoadingIndicator loading={this.state.isLoading} />
                </ImageBackground>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    centerText: {
        flex: 1,
        fontSize: 18,
        padding: 32,
        color: '#777'
    },
    textBold: {
        fontWeight: '500',
        color: '#000'
    },
    buttonText: {
        fontSize: 21,
        color: 'rgb(0,122,255)'
    },
    buttonTouchable: {
        padding: 16
    }
});

export default CustomerComponents;