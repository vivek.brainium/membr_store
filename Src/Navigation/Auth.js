import React, { useState, useEffect } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
const Stack = createStackNavigator();
import LoginScreen from "./../Container/Login/index";
import ForgotPassword from "./../Container/Login/forgotPassword";
import DrawerComponents from "./Drawer";
import AsyncStorage from '@react-native-community/async-storage';
import SplashScreen from "./Splash"

const Auth = (props) => {
  const [UserDetails, setUserDetails] = useState(0);
  useEffect(() => {
    userDetails()
  });

  const userDetails = async () => {
    try {
      const user = await AsyncStorage.getItem('userDetails')
      // console.log("user====>", JSON.parse(user.response_code));
      setUserDetails(user.response_code)
    } catch (e) {
      // saving error
    }
  }

  return (
    <Stack.Navigator
      initialRouteName="Splash"
    >
      <Stack.Screen name="Splash" component={SplashScreen} options={{ headerShown: false }}/>
      <Stack.Screen name="Login" component={LoginScreen} options={{ headerShown: false}} />
      <Stack.Screen name="forgotPassword" component={ForgotPassword} options={{ headerShown: false }} />
      <Stack.Screen name="drawerComponents" component={DrawerComponents}
        options={{ headerShown: false }} />

    </Stack.Navigator>
  )
};
export default Auth;