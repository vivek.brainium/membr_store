import React, { useState, useEffect } from 'react';
import { ImageBackground } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

const SplashScreen = (props) => {
    const [UserDetails, setUserDetails] = useState(0);
    useEffect(() => {
        userDetails()
    });


    const userDetails = async () => {
        // try {
            const user = await AsyncStorage.getItem('userDetails')
            console.log("user====>", JSON.parse(user));
            let userDetail =  JSON.parse(user);
            // const tokenDetails = await AsyncStorage.getItem('userDetails')
            if(userDetail != null){
             // alert("test")
                // setTimeout(() => {
                    props.navigation.replace("drawerComponents")
                // }, 1000)
                
            }else{
                // alert("test11111")
                // setTimeout(() => {
                    props.navigation.replace("Login")
                // }, 1000)
                // props.navigation.navigate("Login")
            }
            // setUserDetails(user.response_code)
        // } catch (e) {
        //     // saving error
        // }
    }

    return (
        <ImageBackground
            source={require("./../Images/Splash_8.png")}
            style={{ flex: 1 }}
            resizeMode={"cover"}
        >

        </ImageBackground>
    )
}

export default SplashScreen;