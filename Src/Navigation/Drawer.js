import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import Ionicons from 'react-native-vector-icons/Ionicons';
import HomeComponents from "./../Container/Home"
import CustomDrawerContent from "./DrawerCoponents"
const Drawer = createDrawerNavigator();
import CustomerScreens from "./../Container/Customer/index"


const DrawerComponents = () => {
    return (
        <Drawer.Navigator
            drawerContent={(props) => <CustomDrawerContent {...props} />}
            initialRouteName="homeComponents"
          
        >
             <Drawer.Screen name="customerScreens" component={CustomerScreens}/>

            {/* <Drawer.Screen name="Login" component={LoginScreen} options={{ headerShown: false }} /> */}
            {/* <Stack.Screen name="forgotPassword" component={ForgotPassword} options={{ headerShown: false }} /> */}
            <Drawer.Screen name="homeComponents" component={HomeComponents} />

        </Drawer.Navigator>
    )
};
export default DrawerComponents;