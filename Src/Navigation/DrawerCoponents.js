import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Animated,
  Image,
  Text,
  StatusBar,
  Keyboard,
  ImageBackground,
  Dimensions,
  TouchableOpacity,
  Alert
} from 'react-native';
import font from "./../Utils/fontSize"
import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-community/async-storage';

class CustomDrawerContent extends React.Component{
    
  state={
    email:"",
    name:""
  }

 async componentDidMount() {
    const user = await AsyncStorage.getItem('userDetails')
    let userDetail = JSON.parse(user);
       this.setState({email:userDetail.response_data.email})
       this.setState({name:userDetail.response_data.name})
}

  
   


  logout = async () => {
    const userData = await AsyncStorage.getItem('userDetails')
    await AsyncStorage.removeItem('userDetails')
    this.props.navigation.replace("Login")
    console.log(await AsyncStorage.getItem('userDetails'), "user data");
  }
  render() {

    return (
    <View style={{ flex: 1 }}>
      <SafeAreaView style={{ flex: 0,backgroundColor: "#0f5054"}} />
      <ImageBackground
        source={require("../../Src/Images/bg.png")}
        style={{ flex: 0.44, }}>
        <View style={{ alignItems: "flex-end" }}>
          <Ionicons
            style={{ padding: 6 }}
            onPress={() => this.props.navigation.closeDrawer()}
            name="md-close-circle-outline"
            size={30}
            color="white" />
        </View>
        
        <View style={{ flex: 1, backgroundColor: Colors.grey,marginTop:40 }}>
      <View style={{  justifyContent: "center", paddingHorizontal: 20, }}>
  <Text numberOfLines={1} style={{ textAlign: "center", color: Colors.white, fontSize: font.extra_large, fontWeight: "800" }}>{this.state.name}</Text>
  <Text numberOfLines={1} style={{ color: Colors.white, fontSize: font.large_size, paddingTop: 20 }}>{this.state.email}</Text>
      </View>
    </View>
      </ImageBackground>

      <TouchableOpacity
        onPress={this.logout}
        style={{ flex: 0.06, flexDirection: "row", alignItems: "center", justifyContent: "center", alignItems: "flex-start", marginBottom: 20,position:"absolute",bottom:5 }}>
        <View style={{ flex: 0.23, alignItems: "center" }}>
          <Image
            style={{ width: 25, height: 25 }}
            source={require("../../Src/Images/logout.png")}
          />
        </View>
        <View style={{ flex: 0.77, alignItems: "flex-start" }}>
          <Text style={{ color: "#00ad6d", fontSize: 18 }}>Logout</Text>
        </View>
      </TouchableOpacity>
    </View>
     );
    }
};

export default CustomDrawerContent;