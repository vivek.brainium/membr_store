import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Animated,
    Text,
    StatusBar,
    Keyboard,
    ImageBackground,
    Dimensions,
    TouchableOpacity,
    Alert,
    Platform
} from 'react-native';
import {
    Header,
    LearnMoreLinks,
    Colors,
    DebugInstructions,
    ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import AntDesign from 'react-native-vector-icons/AntDesign';
import font from "./../Utils/fontSize"
const HeaderComponent = (props) => {

    return (
        <>
          <SafeAreaView style={{ flex: 0, backgroundColor: "#0f5054" }} />
        <View style={{ height: Platform.OS === 'ios' ? 50 : 55, flexDirection: "row", backgroundColor: "#008e6f", alignItems: "center", paddingHorizontal: 20 }}>
            <TouchableOpacity
                onPress={() => props.backPrees()}
            >
                <AntDesign
                    // name="bars"
                    name={props.iconName}
                    size={25}
                    color={Colors.white}
                />
            </TouchableOpacity>

            <Text style={{ color: Colors.white, fontSize: font.large_size, paddingLeft: 20 }}>{props.headerTitle}</Text>
        </View>
        </>
    );
}

const styles = StyleSheet.create({
    scrollView: {
        backgroundColor: Colors.lighter,
    }
});

export default HeaderComponent;