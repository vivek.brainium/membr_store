import React from 'react';
import { View, Text, Image, StyleSheet, ActivityIndicator } from 'react-native';

const LoadingIndicator = (props) => {

    if (props.loading) {
        return (
            <View style={styles.container}>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{ height: 80, width: 80, backgroundColor: '#fff', justifyContent: 'center', alignItems: 'center', borderRadius: 5 }}>
                        <ActivityIndicator size="large" color="#45cfd2" animating={props.loading} />
                    </View>
                </View>
            </View>
        );
    } else {
        return null;
    }
}

export default LoadingIndicator;

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(0,0,0,0.6)',
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: 'center',
        alignItems: 'center'
    },
    logo: {
        width: 60 * 227 / 334,
        height: 60,
    }
});

