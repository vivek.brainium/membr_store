import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Keyboard,
    ImageBackground,
    Dimensions,
    TextInput
} from 'react-native';

import {
    Header,
    LearnMoreLinks,
    Colors,
    DebugInstructions,
    ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
const screenWidth = Dimensions.get('window').width;

import font from "./../Utils/fontSize"


const TextInputComponents = (props) => {
    return (
        <View style={{ flexDirection: "row", alignItems : "center" }}>
           {props.icon && <props.iconName
                name={props.iconType}
                color="#fff"
                size={props.size}
            />}
            <TextInput
                style={{ height: 46, width: screenWidth, paddingLeft: 10, fontSize: font.medium_size, color: Colors.white }}
                onChangeText={(val) => props.onChangeText(val)}
                placeholder={props.placeholder}
                placeholderTextColor={props.placeholderTextColor}
                secureTextEntry={props.secureTextEntry}
            />
        </View>
    )
}

export default TextInputComponents;