import { Network } from "./ApiRequest"

//Network will recieve 4 Arguments
// "method(type of request)",
// "endpoint ()", 
// "body (if POST method)"
// See the main function at ./network.js

export default class Apis {
 
  static login = (data) => {
    return Network('POST', 'store/login', data)
  }
  
}
